package com.aig.dao;

import java.util.Collection;

import com.aig.dto.User;
import com.aig.exception.BussinessException;

/**
 * @author PSingh2
 *
 */
public interface UserDao {
	
	User findOne(int userId) throws BussinessException;

	User create(User user);

	User update(User user);

	void delete(int userId) throws BussinessException;

	Collection<User> findAll();

}
