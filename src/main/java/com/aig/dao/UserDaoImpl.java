package com.aig.dao;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.aig.dto.User;
import com.aig.exception.BussinessException;
import com.aig.repository.UserRepository;

/**
 * @author PSingh2
 *
 */
@Component
public class UserDaoImpl implements UserDao {

	@Autowired
	private UserRepository userRespository;

	/**
	 * Getting User Details based on userId
	 * 
	 * @param userId
	 * @return User Details
	 * @throws BussinessException
	 */
	@Override
	public User findOne(int userId) throws BussinessException {
		return userRespository.findOne(userId);
	}

	/**
	 * Adding new User
	 * 
	 * @param userDetails
	 * @return User Details
	 */
	@Override
	public User create(User user) {
		return userRespository.save(user);
	}

	/**
	 * Updating existing User
	 * 
	 * @param userDetails
	 * @return UserDetails
	 */
	@Override
	public User update(User user) {
		return userRespository.save(user);
	}

	/**
	 * Deleting Existing User
	 * 
	 * @param userId
	 * @throws BussinessException
	 */
	@Override
	public void delete(int userId) throws BussinessException {
		userRespository.delete(userId);
	}

	/**
	 * Returning list of all Users
	 * 
	 * @return UserList
	 */
	@Override
	public Collection<User> findAll() {
		return userRespository.findAll();
	}

}
