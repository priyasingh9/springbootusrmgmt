package com.aig;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author PSingh2
 *
 */
@SpringBootApplication
public class SpringBootUsrmgmtApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootUsrmgmtApplication.class, args);
	}
}
