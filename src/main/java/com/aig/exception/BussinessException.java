package com.aig.exception;

/**
 * @author PSingh2
 *
 */
public class BussinessException extends Exception {

	private static final long serialVersionUID = 1L;

	/**
	 * @param msg
	 */
	public BussinessException(String msg) {
		super(msg);
	}

}
