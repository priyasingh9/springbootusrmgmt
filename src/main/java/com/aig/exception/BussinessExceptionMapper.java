package com.aig.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;

import com.aig.dto.ErrorMessage;

/**
 * @author PSingh2
 *
 */
public class BussinessExceptionMapper implements ExceptionMapper<BussinessException> {

	/**
	 * @param BussinessException
	 * @return Response
	 */
	@Override
	public Response toResponse(BussinessException ex) {
		ErrorMessage errorMessage = new ErrorMessage(ex.getMessage(), 404, "www.google.com");
		Response res = Response.status(Status.NOT_FOUND).entity(errorMessage).build();
		return res;
	}

}
