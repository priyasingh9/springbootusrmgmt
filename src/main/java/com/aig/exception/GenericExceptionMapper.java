package com.aig.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.aig.dto.ErrorMessage;

/**
 * @author PSingh2
 *
 */
@Provider
public class GenericExceptionMapper implements ExceptionMapper<Throwable> {

	/**
	 * @param Throwable
	 * @return Response
	 */
	@Override
	public Response toResponse(Throwable ex) {
		ErrorMessage errorMessage=new ErrorMessage(ex.getMessage(),505,"http://google.com");
		return Response.status(Status.INTERNAL_SERVER_ERROR)
				.entity(errorMessage)
				.build();
		}
	
}
