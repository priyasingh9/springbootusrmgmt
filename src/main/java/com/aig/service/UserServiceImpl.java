package com.aig.service;

import java.util.Collection;

import javax.validation.constraints.NotNull;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aig.dao.UserDao;
import com.aig.dto.User;
import com.aig.exception.BussinessException;

/**
 * @author PSingh2
 *
 */
@Service
public class UserServiceImpl implements UserService {
	final static Logger logger = Logger.getLogger(UserServiceImpl.class);

	@Autowired
	private UserDao userDao;

	/**
	 * Returning list of all Users
	 * 
	 * @return UserList
	 */
	@Override
	public Collection<User> findAll() {
		return userDao.findAll();
	}

	/**
	 * Getting User Details based on userId
	 * 
	 * @param userId
	 * @return User Details
	 * @throws BussinessException
	 */
	@Override
	public User findOne(int userId) throws BussinessException {
		User user = userDao.findOne(userId);
		if (user == null) {
			logger.info("User with userId " + userId + " not Found");
			throw new BussinessException("UserId " + userId + " Not Found");
		}

		return user;
	}

	/**
	 * Adding new User
	 * 
	 * @param userDetails
	 * @return User Details
	 */
	@Override
	public User create(@NotNull User user) {
		logger.info("New User is Added");
		return userDao.create(user);
	}

	/**
	 * Updating existing User
	 * 
	 * @param userDetails
	 * @return UserDetails
	 */
	@Override
	public User update(@NotNull User user) {
		if (user.getUserId() == null) {
			// Cann't update if userId is null
			logger.info("UserId is null Cann't be updated");
			return null;
		}
		logger.info("User with UserId " + user.getUserId() + " is updated");
		return userDao.update(user);
	}

	/**
	 * Deleting Existing User
	 * 
	 * @param userId
	 * @throws BussinessException
	 */
	@Override
	public void delete(int userId) throws BussinessException {
		User user = userDao.findOne(userId);
		if (user == null) {
			logger.info("User with userId " + userId + " not Found, Cann't Delete");
			throw new BussinessException("UserId " + userId + " Not Found, Cann't Delete");
		}

		logger.info("User with UserId " + userId + " is deleted");
		userDao.delete(userId);
	}

}
