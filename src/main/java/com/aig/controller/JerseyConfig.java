package com.aig.controller;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

/**
 * @author PSingh2
 *
 */
@Component
public class JerseyConfig extends ResourceConfig {
	/**
	 * Registering Jersey Controller to the Spring
	 */
	public JerseyConfig(){
		register(UserResource.class);
	}

}
