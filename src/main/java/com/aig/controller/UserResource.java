package com.aig.controller;

import java.util.Collection;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.aig.dto.User;
import com.aig.exception.BussinessException;
import com.aig.service.UserService;

/**
 * @author PSingh2
 *
 */
@Component
@Path("/users")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class UserResource {

	@Autowired
	private UserService userService;

	/**
	 * Returning list of all Users
	 * @return UserList
	 */
	@GET
	public Collection<User> findAll() {
		return userService.findAll();
	}

	/**
	 * Getting User Details based on userId
	 * @param userId
	 * @return User Details
	 * @throws BussinessException 
	 */
	@GET
	@Path("/{userId}")
	public User findOne(@PathParam("userId") Integer userId) throws BussinessException {
		return userService.findOne(userId);
	}

	/**
	 * Adding new User
	 * @param userDetails
	 * @return User Details
	 */
	@POST
	public User create(User user) {
		return userService.create(user);
	}

	/**
	 * Updating existing User
	 * @param userDetails
	 * @return UserDetails
	 */
	@PUT
	public User update(User user) {
		return userService.update(user);
	}

	/**
	 * Deleting Existing User 
	 * @param userId
	 * @throws BussinessException 
	 */
	@DELETE
	@Path("/{userId}")
	public void delete(@PathParam("userId") Integer userId) throws BussinessException {
		userService.delete(userId);

	}
}
