package com.aig.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.aig.dto.User;

/**
 * @author PSingh2
 *
 */

/**
 * UserRepository Extending JPA Repository
 */
public interface UserRepository extends JpaRepository<User, Integer> {

}
