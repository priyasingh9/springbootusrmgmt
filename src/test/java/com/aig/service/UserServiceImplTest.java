package com.aig.service;

import static org.junit.Assert.assertNotNull;

import java.util.Collection;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.aig.SpringBootUsrmgmtApplication;
import com.aig.dto.User;
import com.aig.exception.BussinessException;

/**
 * @author PSingh2
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = SpringBootUsrmgmtApplication.class)
public class UserServiceImplTest {

	
	@Autowired
	UserService userService;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Testing FindOne Method
	 * @throws BussinessException
	 */
	@Test
	public void testFindOne() throws BussinessException {
		User user=userService.findOne(1);
		assertNotNull(user);
	}
	
	
	
	/**
	 * Testing Create Method
	 */
	@Test
	public void testCreate(){
		User user = new User("Anshu.Singh@mindtree.com", "Anshu", 8095201764L, "Female");
		User obj = userService.create(user);
		assertNotNull(obj);
	}
	
	/**
	 * Testing FindAll() Method
 	 */
	@Test
	public void testFindAll(){
		Collection<User> userList = userService.findAll();
		assertNotNull(userList);
	}
	

}
